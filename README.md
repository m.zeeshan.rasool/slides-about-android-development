# Slides about Android Development

Repo of all of my slides related to Android content.


## Suggestions💡

You can suggest a new subject related to Android opening an issue for this repo.

---

## Content

#### Accessibility ♿️

- [More Accessible Apps](./accessibility/more_accessibile_apps_android.pdf) with Homer (The Simpsons)

#### Code Patterns 🧑‍💻

- [Builder](./code_patterns/builder_pattern.pdf) with Commander Data (Star Trek - Next Generation)

- [Factory and Template Method](./code_patterns/factory_and_template_method%20_patterns.pdf) with Mr Neelix (Star Trek - Voyager)

- [List users - Android](./code_patterns/list_users_android%20.pdf)

- [Repository Pattern and Productivity](./code_patterns/repository_pattern_and_productivity.pdf)

- [Singleton and Prototype](./code_patterns/singleton_and_prototype_patterns.pdf) with Yoda (Star Wars)

- [Time Operations on Android](./code_patterns/time_operations_on_android.pdf) with Dr Emmett Brown and Marty Mcfly (Back to the future)

- [Working with Collections - Kotlin](./code_patterns/working_with_collections_kotlin.pdf)

#### Infra 📐

- [Encrypt vs Hashing](./infra/encrypt_vs_hashing.pdf) with Neo, Morpheus, and Trinity (Matrix)

- [Gradle Variables](./infra/gradle_variables.pdf) with Homer (The Simpsons)

- [Mockk and Truth](./infra/mockk_and_truth.pdf) with Spock (Star Trek - Enterprise)

- [My favorite tools](./infra/my_favorite_tools_moro.pdf)

#### UI 🎨

- [Basic Layouts using Compose](./ui/basic_layouts_using_compose.pdf)

- [Constraint Layout in Compose](./ui/constraint_layout_in_compose.pdf) with Homer, and Lisa (The Simpsons)

- [Migrate an Existing App to Compose](./ui/migrate_an_existing_app_to_compose.pdf)

- [RecyclerView and Performance](./ui/recycler_view_and_performance.pdf)

---